package com.Innovaip.Backend;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InnovaipApplication {

	public static void main(String[] args) {
		SpringApplication.run(InnovaipApplication.class, args);
	}

}

// package com.Innovaip.Backend;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.CommandLineRunner;
// import org.springframework.boot.SpringApplication;
// import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

// @SpringBootApplication
// public class BackendForComityAlertsAreaApplication implements
// CommandLineRunner {

// @Autowired
// private BCryptPasswordEncoder passwordEncoder;

// public static void main(String[] args) {
// SpringApplication.run(BackendForComityAlertsAreaApplication.class, args);
// }

// @Override
// public void run(String... args) throws Exception {

// String password = "1234";
// for (int i=0;i<1; i++){
// String passwordBCry = passwordEncoder.encode(password);
// System.out.println(passwordBCry);

// }

// }
// }
