package com.Innovaip.Backend.Domain;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RolsUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max = 20 , message = "El tamaño debe ser entre 2  y 20 caracteres")
    @Column(length = 20)
    private String rol;

}
