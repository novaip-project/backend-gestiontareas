package com.Innovaip.Backend.Domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Projects implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;

    @Size(max = 50, message = "El tamaño del campo debe maximo de 50 caracteres")
    @Column(length = 50)
    private String nombre;

    @Size(max = 300, message = "El tamaño del campo debe maximo de 300 caracteres")
    @Column(length = 300)
    private String descripcion;

    @Size(max = 50, message = "El tamaño del campo debe maximo de 50 caracteres")
    @Column(length = 50)
    private String alias;

    @Column(columnDefinition = "boolean default true")
    private Boolean estado;

    @Column(columnDefinition = "boolean default false")
    private Boolean eliminado;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_inicio", nullable = true)
    private LocalDate fechaInicio;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_fin", nullable = true)
    private LocalDate fechaFin;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_creacion", nullable = true)
    private LocalDate fechaCreacion;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_actualizacion", nullable = true)
    private LocalDate fechaActualizacion;

    @ManyToOne
    private Users user;

}
