package com.Innovaip.Backend.Domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.beans.factory.annotation.Value;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(max = 11, message = "El tamaño del campo debe ser entre 1 y 11 caracteres")
    @Column(unique = true, length = 11)
    private String cedula;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(max = 80, message = "El tamaño del campo debe ser entre 1 y 80 caracteres")
    @Column(length = 80)
    private String nombre;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 5, max = 80, message = "El tamaño del campo debe ser entre 5 y 80 caracteres")
    @Column(length = 80, unique = true)
    private String email;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 5, max = 80, message = "El tamaño del campo debe ser entre 5 y 80 caracteres")
    @Column(length = 80)
    private String password;

    @Column(columnDefinition = "boolean default true")
    private Boolean estado;

    @Column(columnDefinition = "boolean default false")
    private Boolean eliminado;


    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_creacion", nullable = true)
    private LocalDate fechaCreacion;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @Column(name = "fecha_actualizacion", nullable = true)
    private LocalDate fechaActualizacion;
    

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_has_rol", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id"),
            uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "rol_id"})})
    private List<RolsUser> rolsUsers;


    @Column(length = 70, nullable = true)
    private ArrayList authorities;

   

}
