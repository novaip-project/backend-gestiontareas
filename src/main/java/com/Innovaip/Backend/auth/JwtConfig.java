package com.Innovaip.Backend.auth;

public class JwtConfig {

        public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n"
                        + "MIIEpAIBAAKCAQEA0QcOKOQaTVblh8bDNSXeXDA95LE1iGalWiVXir9maxid6KNa\n"
                        + "nuP9PrHcsEYzgtKzDIXLRtEuw2/3O1Av0hoD6OjTIckuN56+WRrxPqTydW6vf8O9\n"
                        + "aHVVTPjX2C6lMTMpaoP/d3kPtkm8p1EsHYTm7t0X63cW2wHkJ2p/KnysBZxMaXQg\n"
                        + "Sq+ONQ8pQwaVx2T/KYbqUkA6amWU9Q/HAIYCMOhxMltKIQVH1ydmFihrvhIhFIvF\n"
                        + "i2/4d/WZvZzPl2YmMI0+7TdU0DxasSP0r4v4Y2rnUm8rcsj21FsmVnX0ozXiHkhy\n"
                        + "L1uGBdqbxXlByzgaJQnlYQdYsYSQ92yLTr98NQIDAQABAoIBADt/1aEAJRiEtrtq\n"
                        + "caCMUfpN5SBW7RURmtnJLV0aVG4ByNkyvpXh8fFJsbcflIQVs8/Jue5DlxscoPRq\n"
                        + "Q+J7c0d2FrtUR+qfLjGxAKDDVRzmT+B/eEfmtkw9vTsVNAKIPK7MOgjpFfbekGvU\n"
                        + "KEmSziWKG8fz0zauGCK8JsySRmkl6zcaEiBWOLEk/8lMk8Nm4D3zKKc+DZeq1A65\n"
                        + "LRS0wXVB6ZwzOR416O8omjt/9Icg5ssdoPRXb7V8DT4IMGISEJvpPqobBiD4twMT\n"
                        + "qoPO/cohnNXmycSmHCMFemrAHUtPERb+zsu3TmwnUVvzGNHeH3XeINNxf6sJouMJ\n"
                        + "LH+VeBUCgYEA/ifK4wHQJK87L4a3ZJPXN+3/v+Ib8BYjjLtQdXAFlF0qpED1Qs2Y\n"
                        + "scWzIAHcvHUwjatVYVnTpBWe7xHQqgb3waRlfglkfiLIe8L32CM8spgkhsqynY7w\n"
                        + "YkW0TLstcZHxvFvpb9HwoV5m3YpCCKA7SxZnUEoDDJXNJ0I69p1DQX8CgYEA0otq\n"
                        + "5K8r/8T1GnzrH80TkTxG+cVydUIRwkXHcfq/DtLUoyC7Iilui+0RSQ5Ey6KN3hAo\n"
                        + "tW7814HV9rVUNAZG6p7D93YA+LzdlAipby4/B8BVD5z4kaFfx+gItyi3ribD1VCj\n"
                        + "L/HwThEbpiTeXvYPKl6FggHditsF0AsuKBECtEsCgYB+B1lyLAn8xvAAZzs1oqIM\n"
                        + "V/aEPilAapoCRThmo8AAjBqDDLsR/0WCkdh7PLEquqny9k0GUzbBnP8M6bahOYdN\n"
                        + "/KlfUGUGr1TfGhUwlniYzxz9wjHEM4c8E/iOPBsSwB4xGxqsmlxKiLdVkCOih2If\n"
                        + "n+JnS+wrJ3VtzEfU1rLtHQKBgQCeOjBnFE5YtUgOU/rPt2sRJt+gLgbKoJCAE7jD\n"
                        + "q9GR7lfnEF4SWli9x7fJ03NYJh2/2Rz+3R4fR2/pLHTPdDgvcomLm583T5il+6x5\n"
                        + "5aNwy9YhGbKkn0ZAEd0a6PrBYliiIyvAiG4z/R9uSR7k9r6G9OoYvhYkmF1R8FUL\n"
                        + "v1zs6QKBgQCiyn3zQsT/jI8nphTFQFzF9afo2CJQ2C1ULkgNPqv2E4ogj12gX5ey\n"
                        + "OjGI92KSz/+SNAlEw3WWIJt7btJwNUXH/RW5AkOIUNRUKCc+vQ4H1U9UGjY1YpVN\n"
                        + "fFA0Q9/2gSFmZpPjcJm2IAgVirdXDceQ0sobwjnpFRBaoVGcfwIRXA==\n"
                        + "-----END RSA PRIVATE KEY-----";

        public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n"
                        + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0QcOKOQaTVblh8bDNSXe\n"
                        + "XDA95LE1iGalWiVXir9maxid6KNanuP9PrHcsEYzgtKzDIXLRtEuw2/3O1Av0hoD\n"
                        + "6OjTIckuN56+WRrxPqTydW6vf8O9aHVVTPjX2C6lMTMpaoP/d3kPtkm8p1EsHYTm\n"
                        + "7t0X63cW2wHkJ2p/KnysBZxMaXQgSq+ONQ8pQwaVx2T/KYbqUkA6amWU9Q/HAIYC\n"
                        + "MOhxMltKIQVH1ydmFihrvhIhFIvFi2/4d/WZvZzPl2YmMI0+7TdU0DxasSP0r4v4\n"
                        + "Y2rnUm8rcsj21FsmVnX0ozXiHkhyL1uGBdqbxXlByzgaJQnlYQdYsYSQ92yLTr98\n" + "NQIDAQAB\n"
                        + "-----END PUBLIC KEY-----";

}
