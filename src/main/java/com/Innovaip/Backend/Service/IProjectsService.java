package com.Innovaip.Backend.Service;

import com.Innovaip.Backend.Service.dto.ProjectsDTO;

import org.springframework.data.domain.Page;

public interface IProjectsService {


    public ProjectsDTO create(ProjectsDTO projectsDTO);

    public Page<ProjectsDTO> read(Integer pageSize, Integer pageNumber);

    public ProjectsDTO update(ProjectsDTO projectsDTO);

    /**
     * this service allows you to get the project details by id
     * 
     * @param id
     * @return
     */
    public ProjectsDTO getById(Long id);

    /**
     * Implement delete service
     * 
     * @param id
     * @return
     */
    public String deleteById(Long id);
    
}
