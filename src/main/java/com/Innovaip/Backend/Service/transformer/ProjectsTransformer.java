package com.Innovaip.Backend.Service.transformer;

import com.Innovaip.Backend.Domain.Projects;
import com.Innovaip.Backend.Service.dto.ProjectsDTO;

public class ProjectsTransformer {

    public static ProjectsDTO getProjectDTOFromProject(Projects project) {

        if (project == null) {
            return null;
        }
        ProjectsDTO dto = new ProjectsDTO();
        /**
         * setting of variables
         */
        dto.setId(project.getId());
        dto.setAlias(project.getAlias());
        dto.setDescripcion(project.getDescripcion());
        dto.setNombre(project.getNombre());
        dto.setEliminado(project.getEliminado());
        dto.setEstado(project.getEstado());
        dto.setFechaActualizacion(project.getFechaActualizacion());
        dto.setFechaCreacion(project.getFechaCreacion());
        dto.setFechaInicio(project.getFechaInicio());
        dto.setFechaFin(project.getFechaFin());
        dto.setUser(project.getUser());

        return dto;
    }

    public static Projects getprojectsFromProjectsDTO(ProjectsDTO dto) {

        if (dto == null) {
            return null;
        }
        Projects project = new Projects();
        /**
         * setting of variables
         */
        project.setId(dto.getId());
        project.setAlias(dto.getAlias());
        project.setDescripcion(dto.getDescripcion());
        project.setNombre(dto.getNombre());
        project.setEliminado(dto.getEliminado());
        project.setEstado(dto.getEstado());
        project.setFechaActualizacion(dto.getFechaActualizacion());
        project.setFechaCreacion(dto.getFechaCreacion());
        project.setFechaInicio(dto.getFechaInicio());
        project.setFechaFin(dto.getFechaFin());
        project.setUser(dto.getUser());

        return project;
    }
}
