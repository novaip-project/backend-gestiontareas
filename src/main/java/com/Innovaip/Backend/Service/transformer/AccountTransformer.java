package com.Innovaip.Backend.Service.transformer;

import com.Innovaip.Backend.Domain.Users;
import com.Innovaip.Backend.Service.dto.AccountDTO;

public class AccountTransformer {

    public static AccountDTO getAccountDTOFromUser(Users user) {
        if (user == null) {
            return null;
        }
        AccountDTO dto = new AccountDTO();

        dto.setId(user.getId());
        dto.setCedula(user.getCedula());
        dto.setNombre(user.getNombre());
        dto.setEmail(user.getEmail());
        dto.setPassword(user.getPassword());
        dto.setEstado(user.getEstado());
        dto.setEliminado(user.getEliminado());
        dto.setFechaCreacion(user.getFechaCreacion());
        dto.setFechaActualizacion(user.getFechaActualizacion());
        dto.setRolsUsers(user.getRolsUsers());
        return dto;
    }
}
