package com.Innovaip.Backend.Service.transformer;

import com.Innovaip.Backend.Domain.Users;
import com.Innovaip.Backend.Service.dto.UsersDTO;

public class UsersTransformer {
    public static UsersDTO getUsersDTOFromUsers(Users user) {

        if (user == null) {
            return null;
        }
        UsersDTO dto = new UsersDTO();
        /**
         * setting of variables
         */
        dto.setId(user.getId());
        dto.setCedula(user.getCedula());
        dto.setNombre(user.getNombre());
        dto.setEmail(user.getEmail());
        dto.setPassword(user.getPassword());
        dto.setEstado(user.getEstado());
        dto.setEliminado(user.getEliminado());
        dto.setFechaCreacion(user.getFechaCreacion());
        dto.setFechaActualizacion(user.getFechaActualizacion());
        dto.setRolsUsers(user.getRolsUsers());

        return dto;
    }

    public static Users getUsersFromUsersDTO(UsersDTO dto) {

        if (dto == null) {
            return null;
        }
        Users user = new Users();
        /**
         * setting of variables
         */
        user.setId(dto.getId());
        user.setCedula(dto.getCedula());
        user.setNombre(dto.getNombre());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setEstado(dto.getEstado());
        user.setEliminado(dto.getEliminado());
        user.setFechaCreacion(dto.getFechaCreacion());
        user.setFechaActualizacion(dto.getFechaActualizacion());
        user.setRolsUsers(dto.getRolsUsers());
        return user;
    }
 
}
