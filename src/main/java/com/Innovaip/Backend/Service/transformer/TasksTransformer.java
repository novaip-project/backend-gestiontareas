package com.Innovaip.Backend.Service.transformer;

import com.Innovaip.Backend.Domain.Tasks;
import com.Innovaip.Backend.Service.dto.TasksDTO;

public class TasksTransformer {
    public static TasksDTO getTaskDTOFromTask(Tasks task) {

        if (task == null) {
            return null;
        }

        TasksDTO dto = new TasksDTO();
        /**
         * setting of variables
         */
        dto.setId(task.getId());
        dto.setNombre(task.getNombre());
        dto.setAlias(task.getAlias());
        dto.setDescripcion(task.getDescripcion());
        dto.setEliminado(task.getEliminado());
        dto.setEstado(task.getEstado());
        dto.setFechaActualizacion(task.getFechaActualizacion());
        dto.setFechaCreacion(task.getFechaCreacion());
        dto.setFechaInicio(task.getFechaInicio());
        dto.setFechaFin(task.getFechaFin());
        dto.setUser(task.getUser());
        dto.setProject(task.getProject());
        dto.setTiempoUsado(task.getTiempoUsado());

        return dto;
    }

    public static Tasks getTasksFromTasksDTO(TasksDTO dto) {

        if (dto == null) {
            return null;
        }
        Tasks task = new Tasks();
        /**
         * setting of variables
         */
        task.setId(dto.getId());
        task.setNombre(dto.getNombre());
        task.setAlias(dto.getAlias());
        task.setDescripcion(dto.getDescripcion());
        task.setEliminado(dto.getEliminado());
        task.setEstado(dto.getEstado());
        task.setFechaActualizacion(dto.getFechaActualizacion());
        task.setFechaCreacion(dto.getFechaCreacion());
        task.setFechaInicio(dto.getFechaInicio());
        task.setFechaFin(dto.getFechaFin());
        task.setUser(dto.getUser());
        task.setProject(dto.getProject());
        task.setTiempoUsado(dto.getTiempoUsado());

        return task;
    }
}
