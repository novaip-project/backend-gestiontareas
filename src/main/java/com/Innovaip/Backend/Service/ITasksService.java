package com.Innovaip.Backend.Service;

import com.Innovaip.Backend.Service.dto.TasksDTO;
import org.springframework.data.domain.Page;

public interface ITasksService {


    public TasksDTO create(TasksDTO tasksDTO);

    public Page<TasksDTO> read(Integer pageSize, Integer pageNumber);
    

    public TasksDTO update(TasksDTO tasksDTO);

    /**
     * this service allows you to get the user details by id
     * 
     * @param id
     * @return
     */
    public TasksDTO getById(Long id);

    /**
     * Implement delete service
     * 
     * @param id
     * @return
     */
    public String deleteById(Long id);

    
}
