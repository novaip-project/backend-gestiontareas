package com.Innovaip.Backend.Service;

import com.Innovaip.Backend.Service.dto.AccountDTO;
import com.Innovaip.Backend.Service.dto.UsersDTO;

import org.springframework.data.domain.Page;

public interface IUsersService {
    
   
    public UsersDTO create(UsersDTO usersDTO);

 
    public Page<UsersDTO> read(Integer pageSize, Integer pageNumber);

  
    public UsersDTO update(UsersDTO usersDTO);

    /**
     * this service allows you to get the user details by id
     * 
     * @param id
     * @return
     */
    public UsersDTO getById(Long id);

    /**
     * Implement delete service
     * 
     * @param id
     * @return
     */
    public String deleteById(Long id);

    /**
     * Get account of UsersDTO
     * 
     * @param email
     * @return
     */
    public AccountDTO getAccountUser(String email);

   
}
