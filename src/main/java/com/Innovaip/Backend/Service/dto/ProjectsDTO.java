package com.Innovaip.Backend.Service.dto;

import java.time.LocalDate;

import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.Innovaip.Backend.Domain.Users;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectsDTO {

  
    private Long id;

    @Size(max = 50, message = "El tamaño del campo debe maximo de 50 caracteres")
    private String nombre;

    @Size(max = 300, message = "El tamaño del campo debe maximo de 300 caracteres")
    private String descripcion;

    @Size(max = 50, message = "El tamaño del campo debe maximo de 50 caracteres")
    private String alias;

    private Boolean estado;

    private Boolean eliminado;

    private LocalDate fechaInicio;

    private LocalDate fechaFin;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

    @ManyToOne
    private Users user;
    
}
