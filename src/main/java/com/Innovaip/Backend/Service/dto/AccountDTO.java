package com.Innovaip.Backend.Service.dto;

import com.Innovaip.Backend.Domain.RolsUser;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AccountDTO implements Serializable{

    private Long id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    private String cedula;

    @NotEmpty(message = "Este campo no puede estar vacio")
    private String nombre;

    @NotEmpty(message = "Este campo no puede estar vacio")
    private String email;

    @NotEmpty(message = "Este campo no puede estar vacio")
    private String password;

    private Boolean estado;

    private Boolean eliminado;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

    private List<RolsUser> rolsUsers;

    @Column(length = 70)
    private ArrayList authorities;
    
}
