package com.Innovaip.Backend.Service.imp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import com.Innovaip.Backend.Domain.Users;
import com.Innovaip.Backend.Repository.UsersRepository;

@Service
public class UsuarioService implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsersRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Users usuario = userRepository.findByEmail(email);

        if(usuario == null){
            logger.error("Error: No existe el usuario" + email);
            throw new UsernameNotFoundException("Error en el login: no existe el usuario" +  email);
        }

        List<GrantedAuthority> authorities = usuario.getRolsUsers()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getRol()))
                .peek(authority -> logger.info("role: " + authority.getAuthority()))
                .collect(Collectors.toList());

        return new User(usuario.getEmail(), usuario.getPassword(), usuario.getEstado(), true , true, true, authorities);
    }

}
