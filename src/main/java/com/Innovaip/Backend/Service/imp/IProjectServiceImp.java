package com.Innovaip.Backend.Service.imp;

import com.Innovaip.Backend.Service.IProjectsService;
import java.time.LocalDate;
import com.Innovaip.Backend.Domain.Projects;
import com.Innovaip.Backend.Repository.ProjectsRespository;
import com.Innovaip.Backend.Service.dto.ProjectsDTO;
import com.Innovaip.Backend.Service.transformer.ProjectsTransformer;
import com.Innovaip.Backend.Service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class IProjectServiceImp  implements IProjectsService{


    @Autowired
    ProjectsRespository respository;


    /**
     * this service allows the creation of task details
     * 
     * @param ProjectsDTO
     * @return
     */
    @Override
    public ProjectsDTO create(ProjectsDTO dto) {

        Projects project = ProjectsTransformer.getprojectsFromProjectsDTO(dto);
        dto.setFechaCreacion(LocalDate.now());
        return ProjectsTransformer.getProjectDTOFromProject(respository.save(project));
    }

    /**
     * this service allows to bring by page the data created from task details
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @Override
    public Page<ProjectsDTO> read(Integer pageSize, Integer pageNumber) {

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return respository.findAll(pageable).map(ProjectsTransformer::getProjectDTOFromProject);
    }

    /**
     * this service allows you to update the data created in task details
     * 
     * @param ProjectsDTO
     * @return
     */
    @Override
    public ProjectsDTO update(ProjectsDTO dto) {
        Projects project = ProjectsTransformer.getprojectsFromProjectsDTO(dto);
        return (ProjectsTransformer.getProjectDTOFromProject(respository.save(project)));
    }

    /***
     * Implement delete service
     * 
     * @param id
     * @return
     */
    @Override
    public String deleteById(Long id) {

        respository.deleteById(id);
        return "has been deleted";
    }

    /**
     * this service allows you to get the task details by id
     * 
     * @param id
     * @return
     */
    @Override
    public ProjectsDTO getById(Long id) {
        Optional<Projects> project = respository.findById(id);
        if (!project.isPresent()) {
            throw new ObjectNotFoundException("Error: El projecto con el id = " + id + " no existe");
        }
        return project.map(ProjectsTransformer::getProjectDTOFromProject).get();
    }
    
}
