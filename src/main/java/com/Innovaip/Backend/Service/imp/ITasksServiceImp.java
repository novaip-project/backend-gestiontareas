package com.Innovaip.Backend.Service.imp;

import java.time.LocalDate;

import com.Innovaip.Backend.Domain.Tasks;
import com.Innovaip.Backend.Repository.TasksRepository;
import com.Innovaip.Backend.Service.ITasksService;
import com.Innovaip.Backend.Service.dto.TasksDTO;
import com.Innovaip.Backend.Service.transformer.TasksTransformer;
import com.Innovaip.Backend.Service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class ITasksServiceImp implements ITasksService{


    @Autowired
    TasksRepository tasksRepository;


    /**
     * this service allows the creation of task details
     * 
     * @param TasksDTO
     * @return
     */
    @Override
    public TasksDTO create(TasksDTO TasksDTO) {
        

        Tasks task = TasksTransformer.getTasksFromTasksDTO(TasksDTO);

        TasksDTO.setFechaCreacion(LocalDate.now());
        return TasksTransformer.getTaskDTOFromTask(tasksRepository.save(task));
    }

    /**
     * this service allows to bring by page the data created from task details
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @Override
    public Page<TasksDTO> read(Integer pageSize, Integer pageNumber) {

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return tasksRepository.findAll(pageable).map(TasksTransformer::getTaskDTOFromTask);
    }

    /**
     * this service allows you to update the data created in task details
     * 
     * @param TasksDTO
     * @return
     */
    @Override
    public TasksDTO update(TasksDTO TasksDTO) {
        Tasks task = TasksTransformer.getTasksFromTasksDTO(TasksDTO);
        return (TasksTransformer.getTaskDTOFromTask(tasksRepository.save(task)));
    }

    /***
     * Implement delete service
     * 
     * @param id
     * @return
     */
    @Override
    public String deleteById(Long id) {

        tasksRepository.deleteById(id);
        return "has been deleted";
    }

    /**
     * this service allows you to get the task details by id
     * 
     * @param id
     * @return
     */
    @Override
    public TasksDTO getById(Long id) {
        Optional<Tasks> task = tasksRepository.findById(id);
        if (!task.isPresent()) {
            throw new ObjectNotFoundException("Error: La tarea con el id = " + id + " no existe");
        }
        return task.map(TasksTransformer::getTaskDTOFromTask).get();
    } 
    
}
