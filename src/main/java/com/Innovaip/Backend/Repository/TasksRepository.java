package com.Innovaip.Backend.Repository;

import com.Innovaip.Backend.Domain.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TasksRepository extends JpaRepository<Tasks, Long> {
    
    Tasks findByNombre(String nombre);

}
