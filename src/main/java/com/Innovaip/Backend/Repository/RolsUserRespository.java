package com.Innovaip.Backend.Repository;

import com.Innovaip.Backend.Domain.RolsUser;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface RolsUserRespository extends JpaRepository<RolsUser, Long>{
    
    List<RolsUser> getFindByRol(String rol);

}
