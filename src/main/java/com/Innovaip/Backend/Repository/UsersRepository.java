package com.Innovaip.Backend.Repository;

import com.Innovaip.Backend.Domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> { 

  
    Users findByEmail(String email);




}
