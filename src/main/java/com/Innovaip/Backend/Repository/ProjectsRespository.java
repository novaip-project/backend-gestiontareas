package com.Innovaip.Backend.Repository;

import com.Innovaip.Backend.Domain.Projects;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectsRespository  extends JpaRepository<Projects, Long>{

    Projects findByNombre(String nombre);
    
}
