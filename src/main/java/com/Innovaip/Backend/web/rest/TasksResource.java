package com.Innovaip.Backend.web.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import com.Innovaip.Backend.Repository.TasksRepository;
import com.Innovaip.Backend.Service.ITasksService;
import com.Innovaip.Backend.Service.dto.TasksDTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
@Api(value = "Web service task", description = "Web service for CRUD task")
public class TasksResource {


    @Autowired
    ITasksService taskService;

    @Autowired
    TasksRepository taskRepository;
    

    /**
     * This method allows you to list tasks by page
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @GetMapping("/tasks")
    @ApiOperation(value = "List task", notes = "Return a task Listed")
    public Page<TasksDTO> read(@PathParam("pageSize") Integer pageSize, @PathParam("pageNumber") Integer pageNumber) {
        return taskService.read(pageSize, pageNumber);
    }


    /**
     * this method allows you to get ttask
     * 
     * @param id
     * @return
     */
    @GetMapping("/task/{id}")
    public ResponseEntity<TasksDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(taskService.getById(id), HttpStatus.OK);
    }

    
   /***
     *This method allows you to create task
     * @param TasksDTO
     * @return
     */
    @PostMapping("/task")
    @ApiOperation(value = "Create task", notes = "Return a task created")
    public ResponseEntity<?> create(@RequestBody TasksDTO tasksDTO, BindingResult result){
        TasksDTO dto = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = taskService.create(tasksDTO);
        } catch (DataAccessException err) {
            response.put("message", "Error al crear un tarea en la base de datos");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "La tarea ha sido creada correctamente");
        response.put("Tarea ", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /***
     * this method allows you to update the data created in task
     * 
     * @param detailtaskDTO
     * @return
     */
    @PutMapping("/task")
    public TasksDTO update(@RequestBody TasksDTO TasksDTO) {
        return taskService.update(TasksDTO);
    }

    /**
     * Implement delete method
     * 
     * @param id;
     */
    @DeleteMapping("/task/{id}")
    public void deleteTask(@PathVariable Long id) {
        taskRepository.deleteById(id);
    }
    
}
