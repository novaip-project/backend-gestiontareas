package com.Innovaip.Backend.web.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import com.Innovaip.Backend.Repository.ProjectsRespository;
import com.Innovaip.Backend.Service.IProjectsService;
import com.Innovaip.Backend.Service.dto.ProjectsDTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
@Api(value = "Web service project", description = "Web service for CRUD project")
public class ProjectsResource {

    @Autowired
    IProjectsService service;

    @Autowired
    ProjectsRespository repository;

    /**
     * This method allows you to list projects by page
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @GetMapping("/projects")
    @ApiOperation(value = "List project", notes = "Return a project Listed")
    public Page<ProjectsDTO> read(@PathParam("pageSize") Integer pageSize, @PathParam("pageNumber") Integer pageNumber) {
        return service.read(pageSize, pageNumber);
    }

    /**
     * this method allows you to get tproject
     * 
     * @param id
     * @return
     */
    @GetMapping("/project/{id}")
    public ResponseEntity<ProjectsDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
    }

    /***
     * This method allows you to create project
     * 
     * @param projectsDTO
     * @return
     */
    @PostMapping("/project")
    @ApiOperation(value = "Create project", notes = "Return a project created")
    public ResponseEntity<?> create(@RequestBody ProjectsDTO projectsDTO, BindingResult result) {
        ProjectsDTO dto = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            dto = service.create(projectsDTO);
        } catch (DataAccessException err) {
            response.put("message", "Error al crear un project en la base de datos");
            response.put("error", err.getMessage() + ": " + err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "El proyecto ha sido creada correctamente");
        response.put("Proyecto ", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /***
     * this method allows you to update the data created in project
     * 
     * @param detailprojectDTO
     * @return
     */
    @PutMapping("/project")
    public ProjectsDTO update(@RequestBody ProjectsDTO projectsDTO) {
        return service.update(projectsDTO);
    }

    /**
     * Implement delete method
     * 
     * @param id;
     */
    @DeleteMapping("/project/{id}")
    public void deleteproject(@PathVariable Long id) {
        repository.deleteById(id);
    }
    
}
